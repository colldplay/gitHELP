# GitHub Self-help

## Local Repo
- create a local copy: `git clone <repository_url>`
- create a local repo of one specific branch: `git clone -b <mybranch> --single-branch <repository_url>` 
- to disable ssl verification _(obviously not recommended)_: 
`git config --global http.sslverify false`
- to update local repository: `git pull`
- if *files* were deleted by accident in the local _repo_: `git checkout HEAD [*path/to/files*]`
- if you want to replace a file on the local repo with the one in the upstream repo: 
`git fetch` then `git checkout origin/[branch_name] [filename]` you have to be on the same directory the [filename] is.

## Committing and pushing 
- check status before pull/push `git status`
- `git add <filename>` - to add a created file to git. File has to be in the current directory 
- `git add *` - add everything
- `git commit -m '<comments>' <filename>` to commit each file separately
- if you make a mistake in commit do `git reset HEAD~`
- if you make a mistake in commit message do `git reset` and `git commit --amend`
- `git commit -a` - to commit everything automatically
- push branch to repository: `git push origin <branch_name>` _git pull has to be performed first to make sure the changes from the remote repo are merged_
- list all committed files: `git ls-tree --full-tree -r HEAD`
- remove file from upstream repo: `git rm --cached [filename]` or `git rm -r --cached [foldername]`. _Don't forget to commit afterwards for changes to take place_
- discard all changes made to the branch before commit but stash them (for possible later use): `git stash` 
- discard all changes made to the branch before commit is made: `git reset --hard`

## Branches
- create new local branch: `git checkout -b <branch_name>`
- delete local branch `git branch -d <branch_name>`
- delete upstream branch `git push origin:<branch_name>`
- create new empty branch not linked to _master_: `git checkout --orphan <branch_name>`
- clear the working directory with:`git rm --cached -r .` 
- `git branch` or `git branch -r` - list all the branches
- `git checkout <branch_name>` - checkout/**switch** a specific branch within the repository 
- pull changes from master branch to your current branch (make sure you are in the current branch first): First commit current changes: `git commit -am "commit before merge"` Then: `git merge origin/master` 
- copy specific files says from master branch to the current branch: make sure you are in the current branch and do `git checkout master -- [filename]`
- to check which files are being tracked by git in current directory in current branch `git ls-tree -r HEAD --name-only`
- rename a branch:
```
git branch -m old_branch new_branch         # Rename branch locally    
git push origin :old_branch                 # Delete the old branch    
git push --set-upstream origin new_branch   # Push the new branch, set local branch to track the new remote
```

## Submodules

Submodules can be usefull when there is a need to have **several repos organized into one** and to make sure that all current changes are pulled

- To add a repo as subrepo: `git submodule add <repo_url>`
- after that you need to initialize and update it: `git submodule init` and `git submodule update`

## Tags
- To select a specific tag within local repo do `git checkout tags/[tag_name]`
- To save committed changes create and switch to new branch: `git checkout -b [new_branch]` 

## Forking
- go to repo you want to fork: _https://github.com/octocat/Spoon-Knife_
- click the __Fork__ button on the top right corner 
- the repository is copied (forked) to your github account
- navigate to your fork and clone it to create a local __repo__ as per usual 
- add the original upstream repo to sync the changes with your local repo: `git remote add upstream https://github.com/octocat/Spoon-Knife.git`  

- To fetch the changes from upstream: `git fetch upstream`
- switch to master branch to sync `git checkout master`
- merge changes `git merge upstream/master`    

## Git info
- `git remote show origin` - to check the remote repo address
- `git log` - check all commit messages
- `git log -p -2` - show difference in **2** most recent commits

- `git diff /directory/file_name` - show difference between local and upstream repo

## Submodules
- `git submodule init` - initialize submodules(other repositories) used in your repository
- `git submodule update` - update the said submodules

## Advanced
- `git remote -v` - list current remote repo address
- `git remote set-url origin [new_url]` - to change the remote repo address
